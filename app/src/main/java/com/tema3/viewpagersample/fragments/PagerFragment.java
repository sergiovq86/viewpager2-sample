package com.tema3.viewpagersample.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.tema3.viewpagersample.R;
import com.tema3.viewpagersample.misclases.Pelicula;
import com.tema3.viewpagersample.viewpager.GalleryAdapter;

import java.util.ArrayList;

/**
 * Created by Sergio on 10/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class PagerFragment extends Fragment {

    private ViewPager2 viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_galeria, container, false);
        viewPager = v.findViewById(R.id.viewpager);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Pelicula> lista = new ArrayList<>();
        lista.add(new Pelicula(R.drawable.img_1, "El Padrino"));
        lista.add(new Pelicula(R.drawable.img_2, "La lista de Schindler"));
        lista.add(new Pelicula(R.drawable.img_3, "Pulp Fiction"));

        GalleryAdapter adapter = new GalleryAdapter(lista, getActivity());
        viewPager.setAdapter(adapter);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                String titulo = lista.get(position).getTitle();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
    }
}
