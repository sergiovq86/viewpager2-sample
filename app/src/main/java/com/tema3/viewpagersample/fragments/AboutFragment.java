package com.tema3.viewpagersample.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.tema3.viewpagersample.AboutMe;
import com.tema3.viewpagersample.R;

/**
 * Created by Sergio on 10/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class AboutFragment extends Fragment {

    private Button bt_more;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_me, container, false);
        bt_more = v.findViewById(R.id.button_more);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bt_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AboutMe.class);
                startActivity(intent);
            }
        });
    }
}
