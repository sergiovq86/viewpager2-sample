package com.tema3.viewpagersample.viewpager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.viewpagersample.R;
import com.tema3.viewpagersample.misclases.Pelicula;

import java.util.ArrayList;

/**
 * Created by Sergio on 10/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Pelicula> arrayList;
    private final Context context;

    public GalleryAdapter(ArrayList<Pelicula> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    static class MyHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.gallery_item, parent, false);
        MyHolder myHolder = new MyHolder(v);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Pelicula peli_en_esta_posicion = arrayList.get(position);
        int image_resource = peli_en_esta_posicion.getImage();

        MyHolder myHolder = (MyHolder) holder;
        myHolder.imageView.setImageResource(image_resource);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
