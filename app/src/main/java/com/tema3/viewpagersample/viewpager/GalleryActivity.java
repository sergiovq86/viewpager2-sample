package com.tema3.viewpagersample.viewpager;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.tema3.viewpagersample.R;
import com.tema3.viewpagersample.misclases.Pelicula;

import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ArrayList<Pelicula> lista = new ArrayList<>();
        lista.add(new Pelicula(R.drawable.img_1, "El Padrino"));
        lista.add(new Pelicula(R.drawable.img_2, "La lista de Schindler"));
        lista.add(new Pelicula(R.drawable.img_3, "Pulp Fiction"));

        setTitle(lista.get(0).getTitle());

        ViewPager2 viewPager = findViewById(R.id.viewpager);
        GalleryAdapter adapter = new GalleryAdapter(lista, this);
        viewPager.setAdapter(adapter);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                String titulo = lista.get(position).getTitle();
                setTitle(titulo);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        //viewPager.setCurrentItem(2);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
