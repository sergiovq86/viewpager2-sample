package com.tema3.viewpagersample.tutorialdinamico;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

/**
 * Created by Sergio on 10/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class DynamicAdapter extends FragmentStateAdapter {

    private final ArrayList<Fragment> lista;

    public DynamicAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle,
                          ArrayList<Fragment> lista) {
        super(fragmentManager, lifecycle);
        this.lista = lista;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return lista.get(position);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void addFragment(Fragment fragment) {
        lista.add(fragment);
        notifyDataSetChanged();
    }

}
