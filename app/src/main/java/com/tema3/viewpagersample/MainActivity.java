package com.tema3.viewpagersample;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tema3.viewpagersample.fragments.AboutFragment;
import com.tema3.viewpagersample.fragments.HomeFragment;
import com.tema3.viewpagersample.fragments.PagerFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                navigateToFragment(id);
                return true;
            }
        });

        navView.setSelectedItemId(R.id.nav_home);
    }

    private void navigateToFragment(int itemId) {
        //fragmen y titulo por defecto
        Fragment fragment;
        // getString es un método de la clase Activity para obtener textos de res/values/strings.xml
        String title = "Home";

        // según la id del item pulsado, creamos ese fragment, y cambiamos el titulo
        switch (itemId) {
            default:
                fragment = new HomeFragment();
                break;

            case R.id.nav_pager:
                title = "ViewPager";
                fragment = new PagerFragment();
                break;

            case R.id.nav_me:
                title = "About Me";
                fragment = new AboutFragment();
                break;
        }

        // transacción del fragment al framelayout
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();

        // asignamos título al toolbar
        setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_about) {
            startActivity(new Intent(this, AboutMe.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}