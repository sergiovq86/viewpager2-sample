package com.tema3.viewpagersample.tutorial;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.tema3.viewpagersample.R;


public class TutorialActivity extends AppCompatActivity {//} FragmentActivity {

    private Button bt_back, bt_forward, bt_end;
    private ViewPager2 viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Paso 1");

        viewPager = findViewById(R.id.viewpager);
        //TutorialAdapter adapter = new TutorialAdapter(this);
        TutorialAdapter adapter = new TutorialAdapter(getSupportFragmentManager(), getLifecycle());
        viewPager.setAdapter(adapter);

        bt_back = findViewById(R.id.bt_back);
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = viewPager.getCurrentItem();
                if (pos > 0) {
                    viewPager.setCurrentItem(pos - 1);
                }
            }
        });

        bt_end = findViewById(R.id.bt_finish);
        bt_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bt_forward = findViewById(R.id.bt_forward);
        bt_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = viewPager.getCurrentItem();
                if (pos < (TutorialAdapter.COUNT) - 1) {
                    viewPager.setCurrentItem(pos + 1);
                }
            }
        });

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setTitle("Paso " + (position + 1));
                switch (position) {
                    case 0:
                        bt_back.setVisibility(View.INVISIBLE);
                        bt_end.setVisibility(View.INVISIBLE);
                        bt_forward.setVisibility(View.VISIBLE);
                        break;

                    case TutorialAdapter.COUNT - 1:
                        bt_back.setVisibility(View.VISIBLE);
                        bt_end.setVisibility(View.VISIBLE);
                        bt_forward.setVisibility(View.INVISIBLE);
                        break;

                    default:
                        bt_back.setVisibility(View.VISIBLE);
                        bt_end.setVisibility(View.INVISIBLE);
                        bt_forward.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        int position = viewPager.getCurrentItem();

        if (position == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseas salir de la guía de configuración?");
            builder.setNegativeButton("No", null);
            builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            builder.create().show();
        } else if (position > 0) {
            viewPager.setCurrentItem(position - 1);
        } else {
            super.onBackPressed();
        }
    }
}
