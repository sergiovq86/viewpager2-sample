package com.tema3.viewpagersample.tutorial;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

/**
 * Created by Sergio on 10/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class TutorialAdapter extends FragmentStateAdapter {

    public static final int COUNT = 3;

//    public TutorialAdapter(@NonNull Fragment fragment) {
//        super(fragment);
//    }

    public TutorialAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

//    public TutorialAdapter(@NonNull FragmentActivity fragmentActivity) {
//        super(fragmentActivity);
//    }



    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new Fragment1();
            case 1:
                return new Fragment2();
            case 2:
                return new Fragment3();
            default:
                return new Fragment1();
        }
    }

    @Override
    public int getItemCount() {
        return COUNT;
    }

}
